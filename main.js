window.addEventListener("load", ()=>{
        fetch("https://apis.is/concerts")
        .then((results)=>{
            return results.json();
        })
        .then((json)=>{
            const mainDiv = document.getElementById("main");
            mainDiv.innerHTML ="";
            for(let i=0; i<json.results.length;i++){
                const date = new Date(json.results[i].dateOfShow)
                let minutes = date.getMinutes()
                    if (minutes < 10) {
                        minutes="0"+ minutes;
                    }
                console.log(json.results[i]);            
                mainDiv.innerHTML += 
                `
                <div class="card">
                    <img src="${json.results[i].imageSource}">
                    <div class="card-text">
                        <h1>${json.results[i].eventDateName}</h1>
                        <h2>${json.results[i].name}</h2>
                        <h3>${json.results[i].dateOfShow}</h3>
                    </div>
                    <button class="info-btn">Skoða nánar<i class="fas fa-angle-right"></i></button>
                </div>
                `
            }

        })
    })
//þegar notandinn ýtir á #send finnur síðan allar buildYard
//á skipum með nafninu sem skrifað hefur verið í #search